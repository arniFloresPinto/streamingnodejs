import express = require('express');
import path = require('path');
import http = require('http');
export default class Server {
   public app: express.Application;
   private port: number;
   private static instance: Server;
   public server: any;

   private constructor(port: number) {
      this.port = port;
      this.app = express();
      this.server = http.createServer(this.app);
   }

   static init(port: number) {
      if(this.instance == null){
         this.instance = new Server(port);
      }
      return this.instance;
   }

   private publicFolder() {
      const publicPath = path.resolve(__dirname, '../../public');
      this.app.use(express.static(publicPath));
   }

   start(callback: Function) {
      this.server.listen(process.env.PORT || this.port, callback());
      this.publicFolder();
   }
}