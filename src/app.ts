import express = require('express');
import morgan from 'morgan';
import cors from 'cors';
import Server from './server/index';
import socketIO from 'socket.io';

const server = Server.init(3000);

server.app.use(morgan('dev'));// For DEV
server.app.use(cors());// For DEV
server.app.use(express.json());
server.app.use(express.urlencoded({ extended: false }));

// Routes

export let io = socketIO(server.server);
require('./server/sockets/socket');

server.start(() => {
   console.log('Runing server 3000 \x1b[32m%s\x1b[0m', 'online');
});