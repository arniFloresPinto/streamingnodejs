var canvas = document.getElementById('preview');
var context = canvas.getContext('2d');

canvas.width = 512;
canvas.height = 384;
context.width = canvas.width;
context.height = canvas.height;

var video = document.getElementById('video');
var socket = io();

function publicMessage(msg){
   document.getElementById('status-webcam').innerText = msg;
}

function loadWebcam(stream){
   video.srcObject = stream;
   publicMessage('Transmitiendo');
}

function errorWebcam() {
   publicMessage('La camara fallo');
}

function showVideo(video, context) {
   context.drawImage(video, 0, 0, context.width, context.height);
   socket.emit('stream', canvas.toDataURL('image/webp'));
   // socket.emit('stream', canvas.toDataURL('image/jpeg', .2));
}

btn.addEventListener('click', ()=>{
   navigator.getUserMedia = (navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia || navigator.msgGetUserMedia);
   if (navigator.getUserMedia) {
      navigator.getUserMedia({
         video: true
      }, loadWebcam, errorWebcam);
   }
   var interval = setInterval(()=>{
      showVideo(video, context);
   }, 70);
});

function closed(){
   location.reload();
}